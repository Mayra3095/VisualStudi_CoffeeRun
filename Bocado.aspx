﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="bocado.aspx.cs" Inherits="Bocado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>¡Una combinación perfecta!</h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="section">
	    	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="products-slider">
							<!-- Products Slider Item -->
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/bocado1.jpg" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Buñuelo</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$500.00
								</div>
								
							</div>
							<!-- End Products Slider Item -->
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/galletas.png" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Galletas</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$1000.00
								</div>
								
							</div>
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/Sandwich-.png" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Sandwich</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$3500.00
								</div>
								
							</div>
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/torta.jpg" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Torta</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$1500.00
								</div>
								
							</div>
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/muffi.jpg" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Muffin</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$2000.00
								</div>
								
							</div>
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/Galleta2.png" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Galleta decorada</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$2500.00
								</div>
								
							</div>
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/pan.png" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Pan picatiso</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$1000.00
								</div>
								
							</div>
							<div class="shop-item">
								<!-- Product Image -->
								<div class="image">
									<a href="page-product-details.html"><img src="img/bocado/pan2.png" alt="Item Name"/></a>
								</div>
								<!-- Product Title -->
								<div class="title">
									<h3><a href="page-product-details.html">Pan galleta</a></h3>
								</div>
								<!-- Product Price -->
								<div class="price">
									$1000.00
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
	    </div>
</asp:Content>

