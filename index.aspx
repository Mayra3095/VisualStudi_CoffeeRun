﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
     <div class="homepage-slider">  
        	<div id="sequence">
				<ul class="img- sequence-canvas ">
					<!-- Slide 1 -->
					<li class="bg14">
						<!-- Slide Title -->
						<h2 class="title">Tu Mejor Cafe</h2>
						<!-- Slide Text -->
						<h3 class="subtitle">El cafe que esperabas, se encuentra aquí</h3>
					</li>
					<!-- End Slide 1 -->
					<!-- Slide 2 -->
					<li class="bg6">
						<!-- Slide Title -->
						<h2 class="title">Arte Cafe</h2>
						<!-- Slide Text -->
						<h3 class="subtitle">Ahora tu cafe tiene arte!</h3>
					
					</li>
					<!-- End Slide 2 -->
					<!-- Slide 3 -->
					<li class="bg13">
						<!-- Slide Title -->
						<h2 class="title">Calidad</h2>
						<!-- Slide Text -->
						<h3 class="subtitle">Original cafe!</h3>
						
					</li>
					<!-- End Slide 3 -->
				</ul>
				<div class="sequence-pagination-wrapper">
					<ul class="sequence-pagination">
						<li>1</li>
						<li>2</li>
						<li>3</li>
					</ul>
				</div>
			</div>
        </div>
    <div class="section section-white">
	    	<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h3>Descripción Del Cafe</h3>
						<p>
							La leyenda dice que hace unos 1300 años un joven pastor árabe notó que sus cabras adquirían una energía extraordinaria luego de comer los frutos de unos arbustos de cafeto. Intrigado cortó unas ramas y las llevó a un monasterio, donde los monjes, después de varios intentos fallidos por conseguir una bebida con los frutos rojos, desistieron y decidieron tirar los granos al fuego. Para sorpresa de todos un agradable aroma comenzó a salir a medida que los granos se quemaban. Así comenzaron a preparara el café y a este fruto le dieron por nombre Kaaba que en árabe quiere decir “Piedra preciosa de color café” los árabes lo cultivaron y lo exportaron de manera exclusiva hasta el siglo XVI y así de Etiopía pasó a Arabia y a la India.

						</p>
						<h3>Visión</h3>
						<p>
							Nuestra visión es ser el café Marca más apasionadamente comprometidos con la calidad y la innovación. Junto con nuestros franquiciados vamos a encender la pasión de nuestros clientes para la experiencia del café perfecto.
						</p>
					</div>
					<div class="col-md-6">
						<img src="img\homepage-slider\slider-bg12.jpg" alt="Responsive image" style="width: 500px;"/>
					</div>
				</div>
			</div>
		</div>
     <div class="section">
			<div class="container">
				<h2>Calidad De Productos</h2>
				<div class="row">
					<!-- Testimonial -->
					<div class="testimonial col-md-4 col-sm-6">
						<!-- Author Photo -->
						<div class="author-photo">
							<img src="img/user1.jpg" alt="Author 1"/>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<!-- Quote -->
								<p class="quote">
		                            Composición del Cafe
		                            <br/>
		                             Cafeína
									 Sales Minerales: Potasio, Magnesio, Calcio, Sodio y Fierro
									 Lípidos
									 Azucares
									 Aminoácidos
									 Vitamina B
									 <br/>
									 Nuestro producto está formado a partir de los granos de café más selectos de la región , son seleccionados por expertos trabajadores del café.


                        		</p>
                        		<!-- Author Info -->
                        		
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
                    <!-- End Testimonial -->
                    <div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<img src="img/user5.jpg" alt="Author 2"/>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                            Los granos de café son tostados y molidos minutos antes de su empaquetadopara asegurarnos de ofrecer un café mucho más fresco y con todos sus nutrientes.Nuestro producto ofrece un sabor inigualable el cual hace la experiencia de beber café una experiencia única e irrepetible, con Café Candela´ es un placer tomar café, desde el olor bien concentrado del grano hasta el sabor más puro y fresco.
                        		</p>
                        		
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
					<div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<img src="img/user2.jpg" alt="Author 3"/>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                             Ofrecemos hasta dos docenas de variedades de café de todo el mundo, recién tostado por tostadores locales. Nuestros asados ​​van desde una completa ciudad luz de caoba asado a un asado italiano oscuro. Diferentes asados ​​poner de manifiesto las características distintivas de cada varietal. Hemos seleccionado varios tostadores de primas en el noreste, que destaca por su calidad y prácticas de compra de conciencia social.
                        		</p>
                        		
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
				</div>
			</div>
	    </div>
        
</asp:Content>

