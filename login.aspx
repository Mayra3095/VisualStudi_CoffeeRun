﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Login</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="basic-login">
                            <div class="form-group">
                                <label for="login-username"><i class="icon-user"></i><b>Username</b></label>
                                <input runat="server" id="textUsername" class="form-control" type="text" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="login-password"><i class="icon-lock"></i><b>Password</b></label>
                                <input class="form-control" id="login-password" type="password" placeholder="">
                            </div>
                            <div class="form-group">
                                <label class="checkbox">
                                    <input type="checkbox">
                                    Remember me
                                </label>
                                <%--<a href="page-password-reset.html" class="forgot-password">Forgot password?</a>--%>
                                <asp:Button ID="buttonLoginUp" runat="server" Text="Login" CssClass="btn pull-right" OnClick="buttonLoginUp_Click" />
                                <div class="clearfix"></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

