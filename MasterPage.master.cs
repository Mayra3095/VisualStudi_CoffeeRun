﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["login"] != null)
        {
            menuAdmin.Attributes["class"] = "active";
            liUsuario.InnerHtml = Session["login"] + "";
            buttonLogin.Text = "Salir";
            buttonLogin.Click += buttonLogin_Click;
            buttonLogin.PostBackUrl = "index.aspx";
        }
        else
        {
            buttonLogin.PostBackUrl = "login.aspx";
        }
    }

    protected void buttonLogin_Click(object sender, EventArgs e)
    {
        Session.Remove("login");
        Response.Redirect("index.aspx");
    }
}

