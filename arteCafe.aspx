﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="arteCafe.aspx.cs" Inherits="ArteCafe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Inspírate y Escoges</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="portfolio-item">
                        <div class="portfolio-image">
                            <a href="page-portfolio-item.html">
                                <img src="img/arte-cafe/artecafe1.jpg" alt="Arte de Cafe" /></a>
                        </div>
                        <div class="portfolio-info-fade">
                            <ul>
                                <li class="portfolio-project-name">HOJA</li>
                                <li>Diseño Especial &amp; Facil de Hacer</li>
                                <li>
                                    <div class="price">
                                        $4.500
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="portfolio-item">
                         <div class="portfolio-image">
                            <a href="page-portfolio-item.html">
                                <img src="img/arte-cafe/arte1.jpg" alt="Arte de Cafe" /></a>
                        </div>
                        <div class="portfolio-info-fade">
                            <ul>
                                <li class="portfolio-project-name">CORAZÓN</li>
                                <li>Diseño Especial &amp; Facil de Hacer</li>
                                <li>
                                    <div class="price">
                                        $4.500
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="portfolio-item">
                        <div class="portfolio-image">
                            <a href="page-portfolio-item.html">
                                <img src="img/arte-cafe/arte2.png" alt="Arte de Cafe" /></a>
                        </div>
                        <div class="portfolio-info-fade">
                            <ul>
                                <li class="portfolio-project-name">ESTRELLA FLOR</li>
                                <li>Diseño Especial &amp; Facil de Hacer</li>
                                <li>
                                    <div class="price">
                                        $4.500
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="portfolio-item">
                         <div class="portfolio-image">
                            <a href="page-portfolio-item.html">
                                <img src="img/arte-cafe/arte3.jpg" alt="Arte de Cafe" /></a>
                        </div>
                        <div class="portfolio-info-fade">
                            <ul>
                                <li class="portfolio-project-name">FLORES</li>
                                <li>Diseño Especial &amp; Facil de Hacer</li>
                                <li>
                                    <div class="price">
                                        $4.500
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="portfolio-item">
                        <div class="portfolio-image">
                            <a href="page-portfolio-item.html">
                                <img src="img/arte-cafe/arte4.jpg" alt="Arte de Cafe" /></a>
                        </div>
                        <div class="portfolio-info-fade">
                            <ul>
                                <li class="portfolio-project-name">CORAZÓN GRANDE</li>
                                <li>Diseño Especial &amp; Facil de Hacer</li>
                                <li>
                                    <div class="price">
                                        $4.500
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="portfolio-item">
                        <div class="portfolio-image">
                            <a href="page-portfolio-item.html">
                                <img src="img/arte-cafe/arte8.png" alt="Arte de Cafe" /></a>
                        </div>
                        <div class="portfolio-info-fade">
                            <ul>
                                <li class="portfolio-project-name">CORAZÓN FLECHADO</li>
                                <li>Diseño Especial &amp; Facil de Hacer</li>
                                <li>
                                    <div class="price">
                                        $4.500
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagination-wrapper ">
        <ul class="pagination pagination-lg">
            <li class="disabled"><a href="#">Previous</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">6</a></li>
            <li><a href="#">7</a></li>
            <li><a href="#">8</a></li>
            <li><a href="#">9</a></li>
            <li><a href="#">10</a></li>
            <li><a href="#">Next</a></li>
        </ul>
    </div>
</asp:Content>

