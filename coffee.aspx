﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="coffee.aspx.cs" Inherits="coffee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">

    <!-- Page Title -->
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>!Seleccione tú café¡</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="eshop-section section">
        <div class="container">
            <div class="row">
                <!-- Sidebar -->
                <div class="col-sm-4 blog-sidebar">
                    <h4>Buscador</h4>
                    <form>
                        <div class="input-group">
                            <input class="form-control input-md" id="appendedInputButtons" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-md" type="button">Buscar</button>
                            </span>
                        </div>
                    </form>
                    <h4>Categorias</h4>
                    <ul class="blog-categories">
                        <li><a href="#">Todos</a></li>
                        <li><a href="#">Café solo</a></li>
                        <li><a href="#">Con crema y/o espuma</a></li>
                        <li><a href="#">Con leche</a></li>
                    </ul>
                </div>
                <!-- End Sidebar -->
                <div class="col-sm-8">
                    <h2>Todos</h2>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/tinto.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>ESPRESSO</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/americano.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>AMERICANO</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/machiato.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>MACCHIATO</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/espresopana.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>ESPRESSO PANNA</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/doble.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>DOBLE</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/cortado.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>CORTADO</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/cafeleche.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>CAFE CON LECHE</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/lagrima.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>LAGRIMA</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="shop-item">
                                <div class="shop-item-image">
                                    <a href="page-product-details.html">
                                        <img src="img/variedades-cafe/capuchino.jpg" alt="Cafe"/></a>
                                </div>
                                <div class="title">
                                    <h3>CAPUCCHINO</h3>
                                </div>
                                <div class="price">
                                    $4.500
                                </div>
                                <div class="actions">
                                    <a href="descripcionCafe.aspx" class="btn btn-small">Descripción</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagination-wrapper ">
                        <ul class="pagination pagination-lg">
                            <li class="disabled"><a href="#">Anterior</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">Siguiente</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Blog Post -->
            </div>
        </div>
    </div>

</asp:Content>

