﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="descripcionCafe.aspx.cs" Inherits="descripcionCafe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Portfolio Item Description</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <!-- Image Column -->
                <div class="col-sm-6">
                    <div class="portfolio-item">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="img/variedades-cafe/cafe1.jpg" alt="Project Name"></a>
                        </div>
                    </div>
                </div>
                <!-- End Image Column -->
                <!-- Project Info Column -->
                <div class="portfolio-item-description col-sm-6">
                    <h3>Project Description</h3>
                    <p>
                        Mauris auctor blandit neque eu cursus. Nunc vel commodo dui, sed tempus mi. Fusce eleifend, orci ut elementum porta, mauris leo porta purus.
                    </p>
                    <p>
                        Etiam aliquet tempor est nec pharetra. Etiam interdum tincidunt orci vitae elementum. Donec sollicitudin quis risus sit amet lobortis. Fusce sed tincidunt nisl.
                    </p>
                    <div class="price">
                        <span class="price-was">$959.99</span> $999.99
                    </div>
                </div>
                <!-- End Project Info Column -->
            </div>
        </div>
    </div>
</asp:Content>

