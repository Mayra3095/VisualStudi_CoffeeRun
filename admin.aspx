﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAdministrador.master" AutoEventWireup="true" CodeFile="admin.aspx.cs" Inherits="admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contenido" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <!-- Action Buttons -->
            <div class="pull-left">
                <h3>Productos</h3>
            </div>
            <div class="pull-right">
                <a href="#" class="btn btn-grey">Deseleccionar</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- Shopping Cart Items -->
            <table class="shopping-cart">
                <!-- Shopping Cart Item -->
                <tr>
                    <!-- Shopping Cart Item Description & Features -->
                    <td>
                        <div class="cart-item-title">CAPUCHINO</div>
                        <div class="feature"><b>Descripción: </b>Es un cafe cualquiera que podemos tomar o botar</div>
                    </td>
                    <!-- Shopping Cart Item ingredientes -->
                    <td>
                        <div class="cart-item-title">Ingredientes</div>
                        <div class="feature">café, crema</div>
                    </td>
                    <!-- Shopping Cart Item type -->
                    <td class="bg-primary type">Cafe</td>
                    <!-- Shopping Cart Item Price -->
                    <td class="price">$4.500</td>
                    <!-- Shopping Cart Item Actions -->
                    <td class="actions">
                        <a href="#" class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="#" class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
                <!-- End Shopping Cart Item -->
                <tr>
                    <!-- Shopping Cart Item Description & Features -->
                    <td>
                        <div class="cart-item-title">CAPUCHINO</div>
                        <div class="feature"><b>Descripción: </b>Es un cafe cualquiera que podemos tomar o botar</div>
                    </td>
                    <!-- Shopping Cart Item ingredientes -->
                    <td>
                        <div class="cart-item-title">Ingredientes</div>
                        <div class="feature">café, crema</div>
                    </td>
                    <!-- Shopping Cart Item type -->
                    <td class="bg-primary type">Cafe</td>
                    <!-- Shopping Cart Item Price -->
                    <td class="price">$4.500</td>
                    <!-- Shopping Cart Item Actions -->
                    <td class="actions">
                        <a href="#" class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="#" class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
                <tr>
                    <!-- Shopping Cart Item Description & Features -->
                    <td>
                        <div class="cart-item-title">CAPUCHINO</div>
                        <div class="feature"><b>Descripción: </b>Es un cafe cualquiera que podemos tomar o botar</div>
                    </td>
                    <!-- Shopping Cart Item ingredientes -->
                    <td>
                        <div class="cart-item-title">Ingredientes</div>
                        <div class="feature">café, crema</div>
                    </td>
                    <!-- Shopping Cart Item type -->
                    <td class="bg-primary type">Cafe</td>
                    <!-- Shopping Cart Item Price -->
                    <td class="price">$4.500</td>
                    <!-- Shopping Cart Item Actions -->
                    <td class="actions">
                        <a href="#" class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="#" class="btn btn-xs btn-grey"><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
            </table>
            <!-- End Shopping Cart Items -->
            <div class="pagination-wrapper ">
                <ul class="pagination pagination-sm">
                    <li class="disabled"><a href="#"><</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row row-form">
        <!-- Form -->
        <div class="col-md-6">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="email" class="form-control" id="textNombre" placeholder="Nombre">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Descripción</label>
                    <input type="password" class="form-control" id="textDescripcion" placeholder="Descripción">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Precio</label>
                    <input type="password" class="form-control" id="textPrecio" placeholder="Precio">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Tipo</label>
                    <select class="form-control">
                        <option>Café</option>
                        <option>Bocado</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="checkbox-inline disabled">
                        <label>
                            <input type="checkbox" disabled="disabled" checked="checked">
                            Café
                        </label>
                    </div>
                    <div class="checkbox-inline">
                        <label>
                            <input type="checkbox">
                            Crema
                        </label>
                    </div>
                    <div class="checkbox-inline">
                        <label>
                            <input type="checkbox">
                            Espuma
                        </label>
                    </div>
                    <div class="checkbox-inline">
                        <label>
                            <input type="checkbox">
                            Leche
                        </label>
                    </div>
                </div>
                <button type="button" class="btn btn-default">Agregar</button>
                <button type="button" class="btn btn-default" disabled="disabled">Modificar</button>
            </form>
        </div>
        <!-- Imagen -->
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputFile">Imagen</label>
                <img src="" alt="Sin seleccionar" class="img-thumbnail center-block">
                <input type="file" id="exampleInputFile">
                <p class="help-block">Solo imagen en formato .jpg</p>
            </div>
        </div>
    </div>
</asp:Content>

